OUTFLANK-MAILMAN
================

Why
---

Mailman likes to modify message headers.  Even if you try to turn off
the message header modification features, it still disassembles and
reassembles the message, which can result in changes to encodings,
reformattings, and so on.  It does not seem to be practical right now
to try to disable all this in mailman.

DKIM (a mail anti-spoofing system whose use is becoming ubiqitous)
depends on messages not being modified in transit.

What
----

outflank-mailman is a "wrapper" around mailman.  Every message that
goes through mailman will go via outflank-mailman -- twice: once on
the way in, and once on the way out.

On input, outflank-mailman saves a copy of the message.  On output,
outflank-mailman replaces the message with the saved copy
(carefully combining it with the headers from the copy that came via
mailman).

The overall result is that the message seen by list subscribers is an
unmodified copy of the message sent by the poster, apart from the
header changes which are part of proper mailing list operation.

Downsides and alternatives
--------------------------

outflank-mailman renders inoperative many mailman configuration
settings.  They still appear in the mailman UI, which could be
confusing, even though outflank-mailman leaves a note in the headers
saying what it has done.

Subject line tags ("Subject: [LIST-NAME] ....") and mailing list
information footers ("to unsubscribe, email ...") can no longer be
provided.  Subscribers must switch to using List-Id for filtering, and
hopefully be using MUAs which look for List-Unsubscribe et al (or
which at least display those headers).

The only reasonably plausible alternative is (selective)
From:-mangling: Mailman can be configured to replace the From: header
of postings with one at its own domain name.  This is highly
questionable.  It breaks reply-to-poster and many other normal ways of
using mailing lists.  It is adding more message mangling to work
around problems caused by message mangling.

INSTALLATION
============

You will need mailman (obviously), perl, sqlite3; including
DBD::SQLite and the sqlite3 command line utility.

Currently outflank-mailman expects to be used with exim4.  It would
not be hard to support other MTAs providing they are flexible enough
to route messages based on header fields.

For running the test suite you'll need faketime.

Currently there is no "distro package".  I suggest you run
outflank-mailman out of a git clone in /usr/local/lib.

General approach
----------------

On input, have outflank-mailman rather than mailman itself hooked into
your system's aliasfile etc.  `outflank-mailman output` acts like a
wrapper program for mailman.

On output, have mailman connect to the local smtp server on a
non-default IP address (or port), and have the MTA check for
appropriate headers etc. and conditionally direct the message to
outflank-mailman.  `outflank-mailman output` will re-invoke the MTA
with the restored copy of the message.

Configuration
-------------

outflank-mailman's message-restoring behaviours are not configurable.

If you want to disapply outflank-mailman for a particular mailing
list, you have several options:

  * arrange for the input messages not to go via `outflank-mailman input`
    (perhaps selecting by recipient address, which will contain the
    list name);
  * arrange for the output messages not to go via `outflank-mailman output`
    (perhaps filtering according to List-Id);
  * have mailman (or something near it) delete
    the `Outflank-Mailman-Id` header.

Configuration of installation matters (program paths etc.) is done via
command line options.  See the command-line reference, below.

Installation instructions, step by step
---------------------------------------

1. Install the script and supporting test tools and docs 
   (by git cloning).

2. Create the directory /var/lib/mailman/outflank to be writeable
   by the user and group `list` (or whatever mailman runs as):
```
     # mkdir -m2770 /var/lib/mailman/outflank
     # chown list:list /var/lib/mailman/outflank
```
   And create the database, as user list:
```
     # really -u list
     $ umask 027
     $ sqlite3 /var/lib/mailman/outflank/db <schema.sql
```

3. Make exim listen on 127.0.0.38 (or some other dedicated localhost
   address).  Typically, this involves changing
   `/etc/network/interfaces` to bring up this address on your loopback
   interface, and then changing `local_interfaces` in the exim
   configuration.  Check that it's working.  (NB there is only one
   IPv6 loopback address (!) so this must be done with IPV4.)

4. Make mailman talk to that 127.0.0.38:25 (e.g.) rather than to
   127.0.0.1:25.  This can be done by putting `SMTPHOST =
   '127.0.0.38'` in `/etc/mailman/mm_cfg.py`.  Restart mailman.

5. Test `outflank-mailman input` (as user `list`):
```
     $ echo 'Foo: hi' | ./outflank-mailman -mcat input
     Outflank-Mailman-Id: 3
     Received: by outflank-mailman (input) for mailman id 3; Thu, 24 Sep 2020 16:16:55 +0100
     Foo: hi
     $
```

6. Change exim config to direct incoming messages to outflank-mailman
   instead of directly to mailman: replace uses of
   `/var/lib/mailman/mail/mailman` with
   `/usr/local/lib/outflank-mailman/outflank-mailman input`
   (or wherever you have put the script).  If your mailman binary is
   elsehere, you must pass outflank-mailman a suitable `-m` option.

7. Make sure the mailing list user `list` is an exim4 "trusted" user
   (so it can use `-oMr`) as otherwise log messages are not very good.
   (Do not make `list` an exim admin user.)

8. Test `outflank-mailman output` (as user `list`):
```
    $ (echo 'Bar: ho'; echo 'Outflank-Mailman-Id: 2'; echo 'List-Post: sponk') | ./outflank-mailman -s./debug-test output dummy
   ./debug-test: -odb -oMr outflank-mailman.2 dummy
Received: by outflank-mailman (output) from mailman id 2 (unknown outflank id, message too old?); Thu, 24 Sep 2020 16:36:43 +0100
Bar: ho
List-Post: sponk
    $
```

9. Add new exim configuration to (i) divert mailman's attempts to send
   mail via outflank-mailman (ii) reject other messages with an
   `Outflank-Mailman-Id`.  For example, these two routers (which
   should appear near the top of your configuration, before the main
   aliasfiles, remote delivery, etc.):

```
  mailman_outflank_good_route:
    driver = manualroute
    condition = ${if and {{def:h_outflank-mailman-id:} {eq {$received_ip_address} {127.0.0.38}}}}
    route_data = t
    transport = mailman_outflank_output_pipe

  mailman_outflank_bad_route:
    driver = manualroute
    condition = ${if def:h_outflank-mailman-id:}
    more = false
    route_data =
    cannot_route_message = unexpected Outflank-Mailman-Id header
```
   and this transport:
```
  mailman_outflank_output_pipe:
    driver = pipe
    initgroups
    user = list
    batch_max = 100
    command = /usr/local/lib/outflank-mailman/outflank-mailman output -f $return_path -- $pipe_addresses
```

10. Add a cron job that runs (daily, say), as user `list`:
```
    /usr/local/lib/outflank-mailman/outflank-mailman expire 32d
```

REFERENCE
=========

Usages
------

 * `.../outflank-mailman` [_options_] `input` [_args for mailman..._]:

   Reads an RFC822-format mail message on stdin; this should be a
   message destined for the local mailman installation.  Preprocesses
   it and then invokes the real mailman.

   The parameters after `input` are the arguments to `mailman`
   (typically, the operation mode and mailing list implied by the
   envelope recipient address).

   The preprocessing is to record the message in the database of
   incoming messages and assign it an ID, and to add an additional
   `Outflank-Mailman-Id` header to the copy passed to mailman.

 * `.../outflank-mailman` [_options_] `output` _args for exim..._

   Reads an RFC822-format mail message on stdin; this should be a
   message which the local mailman installation has decided to send
   out.  Decides what message should really be sent, and then runs
   `/usr/sbin/sendmail` to send it.

   The _args for exim_ should be a `-f` option, `--`, and a sequence
   of recipient addresses (ie, for a list posting, the mailing list
   subscribers).

   outflank-mailman looks to find a List-Post header to confirm that
   the message is in fact a list posting; and it also looks for the
   `Outflank-Maialman-Id` header.  If these are found, and refer to a
   message in the database, outflank-mailman will combine the headers
   from its current stdin with the ones from the database, and use the
   body from the database.  If it restores a message this way, it
   records a copy in a secondary table in the db in case it's needed
   for forensics.

   Otherwise, it uses the message from stdin.

 * `.../outflank-mailman` [_options_] `expire` _days_`d`|_secs_`s`

   Deletes old data from the database.  The maximum age may be
   specified in seconds or days.

   This is a linear search, so it should not be run very frequently.
   Somewhere between 10x and 30x per expiry interval is probably
   sensible.

Options
-------

Option values must be cuddled to the option letter.  Options must
appear before the mode argument (`input`, `output` or `expire`).

 * `-d`_db-file_: Specify the sqlite3 database file to use.

   This file and its containing directory must be writeable by the
   list user.  The file should not be world readable.
   (outflank-mailman sets its umask to 027 to for this reason.)

   The default is `/var/lib/mailman/outflank/db`.

 * `-m`_mailman-program_: Program to run for `mailman`.

   Used by `outflank-mailman input` only.  Default is
   `/var/lib/mailman/mail/mailman` (yes, that is where mailman puts
   its main entrypoint by default).

 * `-s`_sendmail-program_: Program to run for `exim`.

   Used by `outflank-mailman output` only.  Default is
   `/usr/sbin/sendmail`.

   Will be invoked as: _sendmail-program_ `-oi -oee -odb -oMr` `outflank-mailman.`_logid_  _args for exim..._

Processing requirements
-----------------------

**Input processing:**

For outflank-mailman to work, all messages going into mailman widhich
are intended for distribution to the list, must go via
`outflank-mailman input`.

It is harmless, but not desirable, to process for input control
messages, bounce messages, etc.  Probably it is sensible to process
messages for the listowner, since mailman might alter those on the way
through.

**Output processing**

It is also necessary that all list postings going out from mailman are
fed to `outflank-mailman output`.  It is not particularly easy to
distinguish list postings and it is fine and cheap to process all
output from mailman.

So it is probably best to send *all* output from mailman through
`outflank-mailman output`.

**`Outflank-Mailman-Id`**

It is important that no-one can inject messages into your system which
have an `Outflank-Mailman-Id` header.  If someone were to be able to
do that and cause that message to be processed by `outflank-mailman
output` (perhaps by tricking mailman into redistributing the message),
they could arrange to be sent the contents of private messages on
mailing lists they're not subscribed to.

This is why the suggested configuration limits the use of this header
to messages which come directly from mailman on the special loopback
IP address.  Other messages with an `Outflank-Mailman-Id` header have
been (at best) misrouted and should be rejected.

AUTHORSHIP AND LEGAL INFORMATION
================================

outflank-mailman was written by me, Ian Jackson, in my capacity as an
employee of Citrix working on the Xen Project.

outflank-mailman is Copyright 2020 Citrix.

outflank-mailman is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but **WITHOUT ANY WARRANTY**; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received copies of the GNU General Public Licenses v2
and/or v3 along with this program, probably in the files `GPL-2` and
`GPL-3`.  If not, see <http://www.gnu.org/licenses/>.

Formalities
-----------

Individual files generally contain the following tag (or similar)
in the copyright notice, instead of the full licence grant text:
```
  SPDX-License-Identifier: GPL-2.0-or-later
```
As is conventional, this should be read as a licence grant.

Contributions to outflank-mailman are accepted based on the git commit
`Signed-off-by convention`, by which the contributors certify their
contributions according to the Developer Certificate of Origin version
1.1 - see the file DEVELOPER-CERTIFICATE.

If you create a new file please be sure to add an appropriate licence
header, probably something like this:
```
# Copyright Citrix and contributors to outflank-mailman
# SPDX-License-Identifier: GPL-2.0-or-later
# There is NO WARRANTY.
```

For full authorship and change history information, see the git
history.

Compatibility with Mailman licence
----------------------------------

You probably want to use this program with Mailman.  By the GPL's
rules, if you distribute outflank-mailman *with* Mailman, you must
abide by the terms of Mailman's GPL licence for the whole of Mailman
and outflank-mailman together.

For Mailman 3 that means the combination is GPL3+.
