# Copyright Citrix and contributors to outflank-mailman
# SPDX-License-Identifier: GPL-2.0-or-later
# There is NO WARRANTY.

all:

TESTS := $(wildcard tests/*.case)
TEST_OKS := $(addsuffix .ok,$(addprefix tmp/,$(notdir $(basename $(TESTS)))))

check: $(TEST_OKS)

TEST_DEPS = outflank-mailman run-test debug-test

tmp/%.ok: tests/%.case $(TEST_DEPS) | tmp
	./run-test $*

tmp:
	mkdir -p tmp
