-- Copyright Citrix and contributors to outflank-mailman
-- SPDX-License-Identifier: GPL-2.0-or-later
-- There is NO WARRANTY.

CREATE TABLE raw_inputs (
	id		INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	time_t		INTEGER NOT NULl,
	input		TEXT NOT NULL
);

CREATE TABLE mm_outputs (
	subid		INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	id		INTEGER NOT NULL,
	time_t		INTEGER NOT NULl,
	replaced	TEXT NOT NULL
);
